package com.abbtech.user.service;

import java.util.ArrayList;
import java.util.List;
import com.abbtech.user.dto.request.UserRegisterRequestDto;
import com.abbtech.user.entity.Role;
import com.abbtech.user.entity.User;
import com.abbtech.user.repository.RoleRepository;
import com.abbtech.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder bCryptPasswordEncoder;
    private final RoleRepository roleRepository;
    


    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = {BadRequestException.class})
    public void saveUser(UserRegisterRequestDto userRequest) {
       User user =userRepository.findByUserName(userRequest.getUsername());
       if (user ==null){
           user=new User();
           user.setUserName(userRequest.getUsername());
           user.setEmail(userRequest.getEmail());
           user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
       }
        List<Role> roles=new ArrayList<>();
       for (String roleName: userRequest.getRoles()){
           Role role=roleRepository.findByRoleName(roleName);
           if (role==null){
               role=new Role();
               role.setRoleName(roleName.toUpperCase());
               roleRepository.save(role);
           }
           roles.add(role);
       }
       user.setRoles(roles);
       userRepository.save(user);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUserName(username);
    }




}
