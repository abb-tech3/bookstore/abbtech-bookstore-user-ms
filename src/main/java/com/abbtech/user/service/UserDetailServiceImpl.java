package com.abbtech.user.service;

import com.abbtech.user.dto.CustomUserDetails;
import com.abbtech.user.entity.User;
import com.abbtech.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(username);
        System.out.println(user.getRoles());
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        CustomUserDetails userDetails = new CustomUserDetails(user);
        return userDetails;
    }


}
