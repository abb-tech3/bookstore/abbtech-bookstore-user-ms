package com.abbtech.user.controller;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import com.abbtech.user.entity.Role;
import com.abbtech.user.entity.User;
import com.abbtech.user.repository.PermissionRepository;
import com.abbtech.user.service.JWTService;
import com.abbtech.user.service.UserDetailServiceImpl;
import com.abbtech.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/internal")
@RequiredArgsConstructor
public class InternalController {

    private final JWTService jwtService;
    private final UserDetailServiceImpl userDetailService;
    public final UserService userService;
    public final PermissionRepository permissionRepository;

    @GetMapping("/auth")
    public ResponseEntity<Void> auth(@RequestParam(name = "path") String path, @RequestParam(name = "method") HttpMethod method,@RequestParam(name = "token") String token){
        String username = jwtService.extractUsername(token);
        UserDetails userDetails = userDetailService.loadUserByUsername(username);

        if (Boolean.FALSE.equals(jwtService.validateToken(token, userDetails))) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        User user = userService.getUserByUsername(username);
        boolean hasPermission = checkUserPermissions(user, path, method);

        if (!hasPermission) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok()
                .header("X-USER-ID", user.getId().toString())
                .header("X-USER-EMAIL", user.getEmail())
                .build();
    }

    private boolean checkUserPermissions(User user, String path, HttpMethod method) {
        AtomicBoolean permissionIsValid = new AtomicBoolean(false);

        List<Role> roles = user.getRoles();
        roles.forEach(role -> {
            permissionRepository.findByRoleId(role.getId()).forEach(permission -> {
                if (permission.getPath().equals(path) &&
                        permission.getHttpMethod().equalsIgnoreCase(method.name())) {
                    permissionIsValid.set(true);
                }
            });
        });

        return permissionIsValid.get();
    }

    @GetMapping("/test")
    public void test2(@RequestHeader("Authorization") String token,@RequestHeader("X-USER-ID") String id){
        System.out.println("test GET "+ token+ id);
    }


    @PostMapping("/test")
    public ResponseEntity<Void> test3(){
        System.out.println("test POST");
        return ResponseEntity.ok().build();
    }
}
