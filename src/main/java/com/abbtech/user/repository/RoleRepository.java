package com.abbtech.user.repository;

import com.abbtech.user.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
     Role findByRoleName(String roleName);
}
