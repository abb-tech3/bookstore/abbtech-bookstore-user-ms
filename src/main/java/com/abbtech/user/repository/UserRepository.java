package com.abbtech.user.repository;




import java.util.UUID;
import com.abbtech.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUserName(String username);
}
